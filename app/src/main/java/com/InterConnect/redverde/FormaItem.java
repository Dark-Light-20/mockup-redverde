package com.InterConnect.redverde;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class FormaItem extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forma_item);

        String text = "Gran parte del secreto para poder llevar una vida sustentable y ecológica, pasa por la autogestión y autosuficiencia en varios aspectos, está en la alimentación.\n" +
                "\n" +
                "Por eso en ecocosas.com, en este sentido, siempre estamos compartiendo recursos en forma de artículos, vídeos y otros consejos que van desde cómo cultivar zanahorias hasta hacer pan casero, pasando por cómo hacer abonos sin químicos o cómo limpiar de forma ecológica.\n" +
                "\n" +
                "En esta ocasión, una vez más, hablamos del huerto. En este caso del huerto en casa o huerto casero, como se prefiera, para el cual no hace falta gran espacio, puede ser en una terraza un pedazo muy pequeño de tierra, y en este artículo hemos pensado en nuestros lectores que viven en ciudades, por lo cual vamos a hablar de un huerto urbano.\n" +
                "\n" +
                "huerto en casa\n" +
                "No hace falta una gran inversión para armar vuestro propia mini huerto, apenas un poco de información para así dar el puntapié inicial y dar los primeros pasos en la tarea de la siembra y la cosecha.\n" +
                "\n" +
                "Lo primero es contar con un terreno que no tiene por qué ser demasiado amplio pues una huerta para una familia de cuatro o cinco integrantes requiere apenas unos pocos metros cuadrados de superficie.\n";

        TextView textView = findViewById(R.id.forma_item_text);
        textView.setText(text);
    }
}
