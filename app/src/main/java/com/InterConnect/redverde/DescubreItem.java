package com.InterConnect.redverde;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DescubreItem extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descubre_item);

        String text = "1.\tProcura cultivar plantas que te gusten, así te mantendrás interesado en el cultivo\n" +
                "2.\tInfórmate sobre que plantas se pueden sembrar según la temporada en la que te encuentres, ten en cuenta la zona en la que vives.\n" +
                "3.\tVerifica que el suelo de tu cultivo no esté húmedo antes de regar, para evitar la saturación de agua, procura hacerlo en las mañanas, antes de mediodía.\n" +
                "4.\tTrata de producir tus propios fertilizantes a través de compost. \n" +
                "5.\tNo exageres con el uso de productos anti plagas\n" +
                "6.\tInfórmate de las necesidades de las plantas que deseas cultivar, horas de sol, agua, fertilizante etc.\n" +
                "7.\tSi eres principiante una buena forma de empezar es con plantas aromáticas.\n" +
                "8.\tRealiza un acolchado sobre el suelo de siembra (capa protectora del sol) puede ser de compost, pasto seco etc.\n";

        TextView textView = findViewById(R.id.descubre_item_text);
        textView.setText(text);
    }
}
