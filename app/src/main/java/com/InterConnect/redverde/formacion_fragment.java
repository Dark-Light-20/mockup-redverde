package com.InterConnect.redverde;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.InterConnect.redverde.recyclerView.FormaAdapter;

import java.util.ArrayList;

public class formacion_fragment extends Fragment {
    FormaAdapter formaAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view, container, false);

        ArrayList<String> descriptions = new ArrayList<>();
        descriptions.add("¿Cómo usar la red Verde?");
        descriptions.add("¿Cómo hacer un huerto en casa?");
        descriptions.add("¿Cómo hacer una compostera con reciclaje?");
        descriptions.add("¿Cómo hacer compostaje?");
        descriptions.add("¿Cómo Cultivar?");
        descriptions.add("¿Cómo plantar Orégano?");
        descriptions.add("¿Cómo separar los residuos?");

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        formaAdapter = new FormaAdapter(descriptions, getActivity());
        recyclerView.setAdapter(formaAdapter);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        return view;
    }
}
