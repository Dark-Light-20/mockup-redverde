package com.InterConnect.redverde;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.InterConnect.redverde.recyclerView.DescubreAdapter;

import java.util.ArrayList;

public class descubre_fragment extends Fragment {
    private DescubreAdapter descubreAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view, container, false);

        ArrayList<String> descriptions = new ArrayList<>();
        descriptions.add("Consejos para un Huerto casero");
        descriptions.add("¿Cada cuánto tiempo debo regar mi cactus?");
        descriptions.add("Mi planta tiene las hojas secas");
        descriptions.add("¿Químicos u Orgánicos?");
        descriptions.add("¿Cómo evitar el Gorgojo?");
        descriptions.add("¿Cada cuánto tiempo debo fertilizar mis plantas?");
        descriptions.add("¿Mis tomates no crecen?");

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        descubreAdapter = new DescubreAdapter(descriptions, getActivity());
        recyclerView.setAdapter(descubreAdapter);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        return view;
    }
}
