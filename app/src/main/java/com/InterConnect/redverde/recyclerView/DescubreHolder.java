package com.InterConnect.redverde.recyclerView;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.InterConnect.redverde.R;

public class DescubreHolder extends RecyclerView.ViewHolder {
    private TextView description;
    private CardView itemCardView;


    public DescubreHolder(@NonNull View itemView) {
        super(itemView);

        itemCardView = itemView.findViewById(R.id.descubre_item);
        description = itemView.findViewById(R.id.descubre_item_desc);
    }

    public TextView getDescription() {
        return description;
    }

    public void setDescription(TextView description) {
        this.description = description;
    }

    public CardView getItemCardView() {
        return itemCardView;
    }

    public void setItemCardView(CardView itemCardView) {
        this.itemCardView = itemCardView;
    }
}
