package com.InterConnect.redverde.recyclerView;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.InterConnect.redverde.FormaItem;
import com.InterConnect.redverde.R;

import java.util.ArrayList;

public class FormaAdapter extends RecyclerView.Adapter<FormaHolder> {
    private ArrayList<String> descriptions = new ArrayList<>();
    private Activity activity;

    public FormaAdapter(ArrayList<String> descriptions, Activity activity) {
        this.descriptions = descriptions;
        this.activity = activity;
    }

    @NonNull
    @Override
    public FormaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.forma_item, parent, false);
        final FormaHolder formaHolder = new FormaHolder(view);


        formaHolder.getItemCardView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, FormaItem.class);
                activity.startActivity(intent);
            }
        });
        return formaHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FormaHolder holder, int position) {
        TextView desc = holder.getDescription();
        desc.setText(descriptions.get(position));
        holder.getItemCardView().setTag(position);
    }

    @Override
    public int getItemCount() {
        return descriptions.size();
    }
}
