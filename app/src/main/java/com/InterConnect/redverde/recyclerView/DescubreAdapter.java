package com.InterConnect.redverde.recyclerView;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.InterConnect.redverde.DescubreItem;
import com.InterConnect.redverde.R;

import java.util.ArrayList;

public class DescubreAdapter extends RecyclerView.Adapter<DescubreHolder> {
    private ArrayList<String> descriptions = new ArrayList<>();
    private Activity activity;

    public DescubreAdapter(ArrayList<String> descriptions, Activity activity) {
        this.descriptions = descriptions;
        this.activity = activity;
    }

    @NonNull
    @Override
    public DescubreHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.descubre_item, parent, false);
        final DescubreHolder descubreHolder = new DescubreHolder(view);


        descubreHolder.getItemCardView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, DescubreItem.class);
                activity.startActivity(intent);
            }
        });
        return descubreHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DescubreHolder holder, int position) {
        TextView desc = holder.getDescription();
        desc.setText(descriptions.get(position));
        holder.getItemCardView().setTag(position);
    }

    @Override
    public int getItemCount() {
        return descriptions.size();
    }
}
